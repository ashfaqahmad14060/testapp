import React from 'react';
import { StyleSheet, View, TextInput } from 'react-native';

export default class EditText extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={style.container}>
                <TextInput
                    style={style.editText}
                    placeholder={this.props.placeholder}
                >
                </TextInput>
            </View>
        );
    }
}

const style = StyleSheet.create({
    editText: {
        height: 40,
        padding: 10,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#d6d7da'
    },
    container: {
        width: '100%',
        padding:10
    }
})



