import React from 'react';
import { Text, TouchableOpacity, StyleSheet, View } from 'react-native'
import color from "../resources/color.js"

export default class Button extends React.Component {
    constructor(props) {
        super(props);
    }

    render() { 
        return (
            <TouchableOpacity style={style.viewStyle} onPress={this.props.onPressed}>
                <View  >
                    <Text style={style.buttonStyle}>{this.props.title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const style = StyleSheet.create({
    viewStyle: {
        borderRadius: 23,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        width: '70%',
        backgroundColor: '#FFF',
        shadowColor: '#000',
        shadowOpacity: 0.2,
        shadowRadius: 20
    },
    buttonStyle: {
        textAlign: 'center',
        padding: 12,
        fontWeight: 'bold',
        color: 'red'
    }

})