import React from 'react';
import {createStackNavigator, createAppContainer } from 'react-navigation';
import Login from '../screens/Login';

const MainNavigator = createStackNavigator({
    LoginScreen: Login
});

export default createAppContainer(MainNavigator);