import React from 'react';
import { StyleSheet, Text,StatusBar, View ,Button,Toolbar} from 'react-native';
import Navigator from './src/Navigation/AppNavigation';
import Login from './src/screens/Login';

export default class App extends React.Component{

  constructor(props){
    super(props)
   
  }
  render() { 
    return (
      <View style={styles.container}>
        <Navigator/>
        <Login/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
